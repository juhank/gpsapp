package eu.juhan.gpsapp;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import eu.juhan.gpsapp.model.Protos;
import eu.juhan.gpsapp.model.SyncLocation;

/**
 * Main Activity of GPSApp
 * <p/>
 * Most of the code dealing with connecting to GoogleApiClient and requesting location
 * updates is taken from the Google example - Getting Location Updates.
 * https://github.com/googlesamples/android-play-location/tree/master/LocationUpdates
 * <p/>
 * Activity consists of a ListView, ActionBar and Floating Button. The main goal of this
 * activity is record GPS data with a 10 second interval and send GPS data to a mock API
 * with an HTTP request, using Protocol Buffers as the payload.
 * https://developers.google.com/protocol-buffers/
 *
 * @author Juhan Klementi
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener,
		ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	protected final static String LOCATION_KEY = "location-key";
	protected final static String LOCATIONS_KEY = "locations-key";
	protected final static String INDEX_KEY = "index-key";
	protected final static int UPDATE_INTERVAL = 10000;

	protected GoogleApiClient mGoogleApiClient;
	protected LocationRequest mLocationRequest;
	protected Location mCurrentLocation;

	private List<SyncLocation> locations;
	private LocationAdapter adapter;
	// keeps track of the sent and unsent objects in locations array
	private static int lastSentIndex = 0;
	private Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Update values using data stored in the Bundle.
		updateValuesFromBundle(savedInstanceState);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		ListView listView = (ListView)findViewById(R.id.locations_listview);
		adapter = new LocationAdapter(this, locations);
		listView.setAdapter(adapter);

		buildGoogleApiClient();

		// set up handler to send gps data to server periodically
		handler = new Handler();
	}

	/**
	 * Updates fields based on data stored in the bundle.
	 *
	 * @param savedInstanceState The activity state saved in the Bundle.
	 */
	private void updateValuesFromBundle(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
				mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
			}
			locations = savedInstanceState.getParcelableArrayList(LOCATIONS_KEY);
			lastSentIndex = savedInstanceState.getInt(INDEX_KEY);
		} else {
			locations = new ArrayList<>();
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		if (item.getItemId() == R.id.action_clear) {
			locations.clear();
			lastSentIndex = 0;
			adapter.notifyDataSetChanged();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		new DataSender().execute("Button click");
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
		createLocationRequest();
	}

	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();

		// Sets the desired interval for active location updates. This interval is
		// inexact. You may not receive updates at all if no location sources are available, or
		// you may receive them slower than requested. You may also receive updates faster than
		// requested if other applications are requesting location at a faster interval.
		mLocationRequest.setInterval(UPDATE_INTERVAL);

		// Sets the fastest rate for active location updates. This interval is exact, and your
		// application will never receive updates faster than this value.
		mLocationRequest.setFastestInterval(UPDATE_INTERVAL);

		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	/**
	 * Requests location updates from the FusedLocationApi.
	 */
	protected void startLocationUpdates() {
		// The final argument to {@code requestLocationUpdates()} is a LocationListener
		// (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);
	}

	/**
	 * Removes location updates from the FusedLocationApi.
	 */
	protected void stopLocationUpdates() {
		// It is a good practice to remove location requests when the activity is in a paused or
		// stopped state. Doing so helps battery performance and is especially
		// recommended in applications that request frequent location updates.

		// The final argument to {@code requestLocationUpdates()} is a LocationListener
		// (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
		LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	@Override
	public void onResume() {
		super.onResume();
		// Within {@code onPause()}, we pause location updates, but leave the
		// connection to GoogleApiClient intact.  Here, we resume receiving
		// location updates if the user has requested them.

		if (mGoogleApiClient.isConnected()) {
			startLocationUpdates();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
		if (mGoogleApiClient.isConnected()) {
			stopLocationUpdates();
		}
		handler.removeCallbacks(runnable);
	}

	@Override
	protected void onStop() {
		mGoogleApiClient.disconnect();
		super.onStop();
	}

	/**
	 * Runs when a GoogleApiClient object successfully connects.
	 */
	@Override
	public void onConnected(Bundle connectionHint) {

		// If the initial location was never previously requested, we use
		// FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
		// its value in the Bundle and check for it in onCreate().

		// Because we cache the value of the initial location in the Bundle, it means that if the
		// user launches the activity,
		// moves to a new location, and then changes the device orientation, the original location
		// is displayed as the activity is re-created.
		if (mCurrentLocation == null) {
			mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		}

		startLocationUpdates();
		runnable.run();
	}

	/**
	 * Runnable for the repeated task of sending GPS data to server
	 */
	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			long interval = getPref("interval", 0);
			if (interval == 0) {
				handler.removeCallbacks(runnable);
				return;
			}
			try{
				new DataSender().execute("Time");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				handler.postDelayed(this, interval * 1000);
			}
		}
	};

	/**
	 * Callback that fires when the location changes.
	 */
	@Override
	public void onLocationChanged(Location location) {
		Log.d("Location change", location.toString());
		float average = 0;
		float distance = 0;
		if (mCurrentLocation != null) {
			average = mCurrentLocation.distanceTo(location) / ((location.getTime() - mCurrentLocation.getTime()) / 1000);
			distance = mCurrentLocation.distanceTo(location);
		}
		mCurrentLocation = location;
		locations.add(new SyncLocation(location, average));
		adapter.notifyDataSetChanged();
		// send data to server according to distance settings
		long distancePref = getPref("distance", 0);
		if (distancePref != 0 && distance > distancePref) {
			new DataSender().execute("Distance");
		}
	}

	@Override
	public void onConnectionSuspended(int cause) {
		// The connection to Google Play services was lost for some reason. We call connect() to
		// attempt to re-establish the connection.
		Log.i("GoogleApiClient", "Connection suspended");
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// Refer to the javadoc for ConnectionResult to see what error codes might be returned in
		// onConnectionFailed.
		Log.i("GoogleApiClient", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
	}

	/**
	 * Stores activity data in the Bundle.
	 */
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
		savedInstanceState.putParcelableArrayList(LOCATIONS_KEY, (ArrayList<SyncLocation>) locations);
		savedInstanceState.putInt(INDEX_KEY, lastSentIndex);
		super.onSaveInstanceState(savedInstanceState);
	}

	/**
	 * Helper method to get Preference value as String
	 * @param key Key of the preference to retrieve
	 * @param defVal Default value when no value is found for key
	 * @return Value of the Preference as Long
	 */
	private Long getPref(String key, long defVal) {
		String val = PreferenceManager.getDefaultSharedPreferences(this).getString(key, String.valueOf(defVal));
		return Long.valueOf(val);
	}

	/**
	 * Helper class that sends GPS data to the mock API in a background thread
	 * and notifies the adapter if sending failed or not.
	 */
	private class DataSender extends AsyncTask<String, Void, Boolean> {

		private ArrayList<SyncLocation> copy;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			copy = new ArrayList<>(locations.subList(lastSentIndex, locations.size()));
		}

		@Override
		protected Boolean doInBackground(String... params) {
			HttpURLConnection connection = null;
			try {
				Log.d("DataSender", "Sending data - " + params[0]);
				String str = PreferenceManager.getDefaultSharedPreferences(MainActivity.this)
						.getString("server", getString(R.string.url));
				URL url = new URL(str);
				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", "application/octet-stream");
				connection.setReadTimeout(10000);
				connection.setConnectTimeout(15000);

				OutputStream os = connection.getOutputStream();
				for (SyncLocation location : copy) {
					if (location.getStatus() != SyncLocation.Status.SENT) {
						Protos.Location message = Protos.Location.newBuilder()
								.setLatitude(location.getLatitude())
								.setLongitude(location.getLongitude())
								.setSpeed(location.getSpeed()).build();
						message.writeDelimitedTo(os);
					}
				}
				os.close();
				connection.connect();

				return connection.getResponseCode() == 200;

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (connection != null)
					connection.disconnect();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean success) {
			synchronized (locations) {
				for (int i = lastSentIndex; i < locations.size(); i++) {
					locations.get(i).setStatus(success ? SyncLocation.Status.SENT :
					SyncLocation.Status.FAILED);
				}
			}
			if (!success) Toast.makeText(MainActivity.this, "Sending failed", Toast.LENGTH_SHORT).show();
			else lastSentIndex = copy.size();
			adapter.notifyDataSetChanged();
		}
	}
}