package eu.juhan.gpsapp.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Object that represents one item in the ListView of MainActivity.
 * <p/>
 * The average field holds the average speed that was recorded from the previous
 * location to this location. Status determines if the Location is displayed as
 * black (UNKNOWN), green (SENT) or red (FAILED) in the ListView.
 *
 * @author Juhan Klementi
 */
public class SyncLocation implements Parcelable {

	private float average;
	private Location location;
	private Status status;

	public enum Status {
		UNKNOWN,
		SENT,
		FAILED
	}

	public SyncLocation(Location location, float average) {
		this.location = location;
		this.average = average;
		status = Status.UNKNOWN;
	}

	protected SyncLocation(Parcel in) {
		location = in.readParcelable(Location.class.getClassLoader());
		status = Status.valueOf(in.readString());
		average = in.readFloat();
	}

	public static final Creator<SyncLocation> CREATOR = new Creator<SyncLocation>() {
		@Override
		public SyncLocation createFromParcel(Parcel in) {
			return new SyncLocation(in);
		}

		@Override
		public SyncLocation[] newArray(int size) {
			return new SyncLocation[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(location, i);
		parcel.writeString(status.name());
		parcel.writeFloat(average);
	}

	public double getLatitude() {
		return location.getLatitude();
	}

	public double getLongitude() {
		return location.getLongitude();
	}

	public float getSpeed() {
		return location.getSpeed();
	}

	public long getTime() {return location.getTime();}

	public float getAverage() {return average;}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status sent) {
		this.status = sent;
	}
}
