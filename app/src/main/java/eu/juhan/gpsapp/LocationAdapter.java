package eu.juhan.gpsapp;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import eu.juhan.gpsapp.model.SyncLocation;

/**
 * Adapter that binds the data of GPS Locations to the ListView of MainActivity
 *
 * @author Juhan Klementi
 */
public class LocationAdapter extends BaseAdapter {

	private List<SyncLocation> locations;
	private Context context;

	public LocationAdapter(Context context, List<SyncLocation> locations) {
		this.locations = locations;
		this.context = context;
	}

	@Override
	public int getCount() {
		return locations.size();
	}

	@Override
	public SyncLocation getItem(int position) {
		return locations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SyncLocation location = getItem(position);
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.listview_item, parent, false);
			holder.time = (TextView) convertView.findViewById(R.id.time_textview);
			holder.coordinates = (TextView) convertView.findViewById(R.id.coordinates_textview);
			holder.speed = (TextView) convertView.findViewById(R.id.speed_textview);
			holder.average = (TextView) convertView.findViewById(R.id.average_textview);
			convertView.setTag(holder);
 		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date(location.getTime()));
		holder.time.setText(currentDateTimeString);
		holder.coordinates.setText(location.getLatitude() + ", " + location.getLongitude());
		DecimalFormat format = new DecimalFormat("#.#");
		holder.speed.setText(format.format(location.getSpeed()) + "m/s");
		holder.average.setText(format.format(location.getAverage()) + "m/s");


		// set text to green if it is sent to server
		switch (location.getStatus()) {
			case SENT:
				int green = ContextCompat.getColor(context, R.color.green);
				holder.coordinates.setTextColor(green);
				break;
			case FAILED:
				int red = ContextCompat.getColor(context, R.color.red);
				holder.coordinates.setTextColor(red);
				break;
		}
		return convertView;
	}

	static class ViewHolder {
		TextView time;
		TextView coordinates;
		TextView speed;
		TextView average;
	}
}
