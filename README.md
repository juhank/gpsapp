GPSApp
==============

Description
--------------

This Android application is a small demo app that starts listening 
to Locations from the GPS sensor and displays them in a List. It sends 
data to a mock API in the form of Protocol-Buffer (https://developers.google.com/protocol-buffers/)
messages according to Time or Distance settings or by pushing the Floating Action Button. 

Install
--------------

- Clone this git repository and import project in Android Studio or any other IDE. It is also possible to use gradle and build using the command line.
- You can also install the app by installing the APK that is included in the repository with ADB or open the link in your mobile 
browser (You have to have untrusted sources switched on from the Settings in your Android phone).

Proto files
--------------

The only .proto file described in the project is Location.proto in the app/ directory. Protos.class was generated using proto buffers compiler protoc version 2.6.1 and the protobuf library jar was compiled with java version 1.7. 

TODO
--------------
As this is a demo project and is far from complete, some things were left undone to save time. Some things to consider doing (may be incomplete):

- Testing
- Background service for tracking locations or wakelock
- Saving unsent locations to file or SavedPreferences or Database
- Handle permissions according to spec of the SDK 23
- Improve error handling 
- Add support to change tracking interval from the Settings
- Add HTTPS support

